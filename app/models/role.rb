class Role < ActiveRecord::Base
  has_many :users

  def admin?
    name == "admin"
  end

end
