class User < ApplicationRecord
  belongs_to :role, optional: true
  has_many :ideas

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  delegate :admin?, to: :role

  def admin?
    role&.admin?
  end


end