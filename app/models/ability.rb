class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:

    user ||= current_user # guest user (not logged in)

    if user.admin?
      can :manage, :all
    else
      can :update, Idea do |idea|
        idea.user == user

      end
      can :destroy, Idea do |idea|
        idea.user == user
      end
      can :create, Idea
      can :read, :all # permissions for every user, even if not logged in
    end
  end
end
