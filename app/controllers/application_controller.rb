class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end
  # Catch all CanCan errors and alert the user of the exception
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

end

